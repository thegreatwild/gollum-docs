## v3.14.0

* ### [Tests Conducted](https://docs.google.com/spreadsheets/d/1Th1IExQaXLVkPmgjLIjOf2UBD9GArHyj4TdWlj7B20Y/edit?usp=sharing)

* ### Bugfixes
   * Delete button icon do not appear anymore on comments you do not own
   * The comment do not stay in the comment box upon submission of a comment while waiting for a server response
   * Like statistics now appear when viewing a specific feed item
   * "Liked by" text sequence is now fixed

* ### Features Added/Changed
   * Hide profile images in hunt summary, detailed hunt ad, and ad preview views for now
   * Adjust transparency in Map View of Find Hunts
   * Numpad keyboard is now used instead of a full keyboard when inputting price and number of guests in create hunt view

## v3.13.0

* ### [Tests Conducted](https://docs.google.com/spreadsheets/d/1jGcOkfguheq4V_vxSCCTV0gUv8CABL4GG8_4ygNOA4M/edit?usp=sharing)

* ### Features Added/Changed
  * Create Hunt Process has been simplified
  * Removed payment forms
  * Sending a newsfeed comment disables the button until the server has responded. Also automatically adds the comment to the feed item as dummy comment with a '-1' id.

* ### Bugfixes
  * Date Input for Large Screen (Web and Tablet) now fixed

* ### Hotfixes
  * Price Per Person when creating a hunt.
     * The server returns an added 10% value to the declared price so the client is currently deducting the 10% to show the original price.
  * Type of hunts in Create Hunt Process
     * Currently, IDs less than 100 are being filtered out since we have a new set of options for Type of Hunts. 
  * Commenting times out
     * A dummy comment is added right away upon submitting a comment to a newsfeed item. This dummy comment has an id of '-1' which is when deleted, will not be recognized by the server. This dummy comment however gets removed once the server has replied.
  * Bank Account Details
     * The server requires bank details to be included when submitting a new hunt and submitting a booking. A test card is currently being used to supply this for the sake of letting the server accept it.

## v3.12

* ### [Tests Conducted](https://docs.google.com/spreadsheets/d/1aaUArNknm0GWH2HU_9HINKxv8VbKDT1DJc13MGSVK9w/edit?usp=sharing)

* ### Features added/changed
  * Removed + Sign Overlay Button in News Feed View for Web.
  * Removed + Sign Overlay Button in My Hunts View for Web.
  * Removed + Sign Overlay Button in Find Hunts View for Web.
  * Removed + Sign (Menu - Top Right) Button in News Feed View for Mobile.
  * Removed + Sign (Menu - Top Right) Button in My Hunts View for Mobile.
  * Removed + Sign (Menu - Top Right) Button in Find Hunts View for Mobile.
  * Added Action Menu Bar in News Feed View (Trophy & Observation buttons).
  * Added a dummy Status Bar to be able to create a Status post in News Feed View.
  * Added Action Menu Bar in My Hunts View (Invite to Hunt button).
  * Create Observation and Trophy process has been simplified.
  * New layout for the Mini Hunt Ad.
  * Loading Bar is now shown upon submitting a booking.
  * Fixed Hunt Team News Feed.
  * Changed Color of Filter Button in Find Hunts View from Black to Orange (see SASS TGW Defined Colors).
  * Added a back button upon clicking a Mini Hunt Ad on My Hunts View.
  * (Mobile Only) Enlarged map on Create Hunt Team View.
  * Added translations.

* ### Bugs discovered
  * Date Input doesn't seem to work on Safari and Firefox, revert this to Custom Web Version on Next Release.
  * Mini Hunt Ads in My Hunts View are distorted in iOS 8 and 9 as well as Android 4 and 5.
  * Back Button on Create Status View appears on all platforms EXCEPT iOS.

## v3.11.22

* ### Hotfix
  * Newsfeed used to not retrieve new newsfeed items upon opening the app.

## v3.11.21

* ### [Tests Conducted](https://docs.google.com/spreadsheets/d/1t-aZC4VnMN0TJssP70FKGFv1xegcP8CulX6iGUg_e7A/edit?usp=sharing)

* ### Features added/changed
  * New public hunt view.
  * New background and splash screen images.
  * Translations have been localized to the app.
  * Can now navigate back to newsfeed from a user profile you selected (viewed) on the newsfeed.

* ### Features removed/on-hold
  * Users who are already using the app won't show in the Invite Friends Facebook Dialog.
  * Removed Facebook Invite Friends Button. (Temporary until 3rd party has fixed the issue)

* ### Bugfixes
  * Logged in user's own messages in the bubble chat are now left aligned.
  * Blank messages are not created anymore when you try to "Send Message" to a person you've never had a conversation before.
  * Access token expiry has been extended from 30 days to 120 days.

* ### Deployment changes
  * Web App is now hosted using AWS EB Instead of AWS S3.
  * Deploy script creates a zip file to be manually uploaded and deployed to AWS EB.
  * Deploy script now retains the Facebook plugin and only updates it instead of downloading it again for every "reinstall".
  * html5modeConstant.js must exist with the new implementation. It's the "switch" between handling the URL differences of the Web App and Mobile App.

## v3.10

* ### [Tests Conducted](tests/v3-dot-10-dot-0)

* ### Bugfixes
  * When the app is in background state for more than 3 minutes and is brought back to foreground (with News Feed as the last activity), the News Feed is automatically refreshed.
  * Any push notifications received, except push notifications from private messages, that is opened will redirect you to a refreshed News Feed.
  * Fixed broken URL for User Agreement in Settings.
  * Images in News Feed now respect the aspect ratio of the retrieved image in favor of a requested resized 720p (720x1280) 16:9 image instead of a cropped 720x540 4:3 image from the image server.

* ### Changes
  * Updated **cordova-plugin-camera** from **[v1.1.0](https://github.com/apache/cordova-plugin-camera/releases/tag/1.1.0)** to **[v2.0.0](https://github.com/apache/cordova-plugin-camera/releases/tag/2.0.0)**
  * Updated **cordova-plugin-inappbrowser** from **[v1.0.1](https://github.com/apache/cordova-plugin-inappbrowser/releases/tag/1.0.1)** to **[v1.3.0](https://github.com/apache/cordova-plugin-inappbrowser/releases/tag/1.3.0)**
  * Updated **iOS Facebook Plugin** from **[phonegap-fb-plugin](https://github.com/uofmmike/phonegap-facebook-plugin)** to **[cordova-plugin-fb4](https://github.com/jeduan/cordova-plugin-facebook4)**