1.    Make sure the setup for the market locally with the docker images ‘composed’ by docker daemon
2.    Compose stress tests and try to ‘crash’ the local OR elasticbeanstalk ‘dev-market’. For this switch, the environment configuration is modified between 'local' or 'dev'
* Create ‘dummy’ users and associated Friends (also users) in tgw-dev-api database
* * To create these users, just create an SQL stored procedure to create these dummy user records
* * Create dummy friends via appropriate sql queries
* Each user in a. has n number of friends above 100+ (e.g. 100, 300, 500, so on so forth)
* On local front end setup, create a POST(s) from a logged in user with n number of friends. This did NOT crash.
* On local front end setup, create a POST(s) from different logged in users (that is, from different browsers) and interleaved in doing the posts to simulate similar to what a real situation where different users are posting at varying times. This will crash the dev-market server. 
* On local front end setup, create prototype code to simulate a logged user looping a post with IMAGE content 100, 200, 300, n times. This will crash the dev-market server.
* On local front end, simulate a notification storm similar to what was done to this code
* Storm notification using the tgw-app component, tgw\tgw-app\src\log\status\status.ctrl.js , inject this ‘test code’ to produce a notification storm

$scope.submit = function() {
        if (submitted) {
            return false;
        } else if (!$scope.status.content) {
            flash.info('EMPTY_CONTENT_ERROR');
            return false;
        }

        submitted = true;
        var user = UserService.getUser();
        var submitAction = $rested().all('posts');

        if ($scope.edit) {
            var id = $scope.status.id;
            submitAction = $rested().all('posts').one(id);
        }

        if ($scope.status.content) {
            $scope.status.content = stripTags($scope.status.content);
        }

        // Append the image to the status array
        angular.extend($scope.status, {
            image: $scope.image
        });

       
        // Saves the status in the database
       //wrap the logic in for loop, where 100 is the number of times
       //the post is supposed to be done consequently
        for(var i=1; i <= 100;i++)
        {
            submitAction.save($scope.status).then(function(response) {
                $log.log('Post response ' + response);
                MixpanelService.track('Status Uploaded');

                
                    // If the user edit's then delete the item from cache before adding
                    if ($scope.edit) {
                        $rested().all('feed').deleteCacheItem(id);
                    }

                    /* Creates the status post in the cache so it will be visible
                    when the user enters the feed view */
                    $rested().all('feed').prependCache({
                        id: response.id,
                        comment_count: response.comment_count,
                        content: response.content,
                        image: response.image,
                        item_type: response.type,
                        item_url: 'feed/' + response.id,
                        like_count: response.like_count,
                        max_likes: 1,
                        liked_by: {
                            me: undefined,
                            friends: [],
                            others: []
                        },
                        timestamp: response.created_at,
                        user: {
                            first_name: user.first_name,
                            id: user.id,
                            image: user.image,
                            name: user.name
                        }
                    });

                    $app.trigger('clearHistory');
                    //$state.go('feed');
					
                
            }, function(error) {
                $log.log('Post error ' + error);
                if ($scope.edit) {
                    flash.error('STATUS_UPDATE_ERROR');
                } else {
                    flash.error('STATUS_POST_ERROR');
                }
            });
        //end of the loop
        }
    };

![latold](images/LatencyOld.png)

* * As shown on the average latency graph from the monitoring panel, running the notification storm test code basically generates 60-sec latency per request coming into the server. Validating it against an actual browser interface produces 504 (Gateway timeouts) rendering the application and website unresponsive.
* * Conclusion of the dev-market server crash in found on the bundle logs.
* * The logs show push notification successful and FAILED attempts.  We draw some info and found out that there are failed attempts. Image below shows attempts and a custom generated ERROR message (highlighted by the red box below). At the bottom of this ERROR message shows a library exception caused by the Reactive component
![er1](images/err1.png)
* * From the same error log, ‘Couldn’t send message after 3 retries’… led for the assumption that if a notification object failed the first time and the system TRIES to push or send it through for at least three (3) times, then it is possible that for 100 notifications to friends (assuming each failed the first time), it could reach 300 attempts overall. This number could mean the system could not handle the amount and just started to respond erratically
![er2](images/err2.png)

3.    Based on #2 error findings, the following steps were taken based on assumptions made
* Architecture Information on the Market (Java) Code Base
* * Starting point for the application is \hunting-ad\src\main\java\com\tgw\web\Application.java
* * PersistenceModule has dependencies on RedisModule. RedisModule has dependency on JedisPoolProvider, RedisCache, RedisEventBus.
* * PropertiesModule and RepositoriesModule
* * ServiceModule and related objects include: OnMessageObserver and PushNotificationsObserver
* * * OnMessageObserver has a dependency on NotificationMessageMapper and PushNotificationsService (both for android and apple). OnMessageObserver is a key player as this imlements the ‘onNext’ method of a subscriber/observer. OnNext method means that the observable has ‘emitted’ an item and the observer reacts accordingly.
* * * This is most likely one of the collaborators on the design that is involved in the market crash. Again this is on the basis of the error logs collected EVERY TIME a crash happens on the production environment and dev-market environment as well.
* * * Look into the installation implementation of the ApplePushNotificationsModule and the AndroidPushNotificationsModule. Each is bound to ApplePushNotificationsService and AndroidPushNotificationsService respectively. These objects implement the “push” method that creates the observable object and correlated objects. Will have to find out exactly what the operators do in this code.
* * * The PushNotificationsObserver object handles emissions is a composition that handles the MessageObserver’s onNext method. Look into this code: _“eventBus.consumer(pushChannels.channels()).onMessage(messageObserver::onNext)"_

* * UseCasesModule
* * VerticleModule and dependencies
* * *  VertxProvider - Uses the Vertx library that is implemented to as event-based, reactive apps
* * *  VerticleLaunchable
* * * MainVerticle has dependencies that could be responsible or not at all but still subject for investigation ( AppEventsVerticle, UserEventsVerticle, WorkerVerticle)
* * * *  When initialized, it uses the RxVertx object that is a combination of Rx components involved in the functional behavior. This is part of the Vertx Reactive (Observables) approach. (IMO, totally new and ‘alien’ and will need to catch up with what the heck it’s all about)
* * * * The ‘start’ method implements the creation of the application server, the request handlers, so on and so forth. It’s possible the answer lies here because the Observables are basically initialized here (implemented on the derived objects, that is, UserEventsVerticle, AppEventsVerticle, WorkerVerticle) . The observers/subscribers, event bus, consumer, and related RX operators are configured. No certainty in the implementation so resources to catch up are the following:
* * * * * http://reactivex.io/documentation/observable.html
* * * * * http://reactivex.io/documentation/operators.html
* * * * * http://reactivex.io/documentation/scheduler.html
* * * * * http://docs.couchbase.com/developer/java-2.0/observables.html

* * RxModule and dependencies on RxEventBusProvider and RxVertxProvider

* Modify and implement debugging code to the suspected ‘collaborator’ objects to track the objects managed in the functional behavior(s) involved.
* * Identify the workflow for creating a new build of the ROOT.war file once code modifications are made on the java source code.
* * The workflow elements include the docker images, containers, and commands that needed to be updated as well in order to reflect the new changes locally.
* * The following steps are:
* * * When changing code base and applying those changes to the backend-container, on “/setup” directory, execute on docker terminal  docker-compose up to rebuild, BUT make sure stop and remove the container ‘full_java-backend’ using ‘rm’, then remove the image corresponding also with ‘rmi’
* * * Run the docker-compose up from “/full” directory to REBUILD the docker image for the full-java backend docker container.
* * * Important NOTE:  Before executing #2, make sure the updated ROOT.war is on the “/docker” directory, otherwise an error similar to this is shown: **Service ‘java-backend’ failed to build: lstat ROOT.war: no such file or directory**

* Deployment on Amazon’s Elastic Beanstalk took the following steps. NOTE: the documentation on the stash repo required some CLI work and Elastic beanstalk cli knowledge. As discussed, it could be dangerous to do so without the right information and background on how to do it so for this step, the option to do it manually through the Elastic Beanstalk Application Configuration service was performed instead.
* * Select the docker.aws.json, Dockerfile, and ROOT.war, then compress as zipped file. DO not select the parent folder and zip. Issues with the latter option will cause an error when the dockerfile and docker.aws.json COULD NOT BE FOUND when the AWS eb service will try upload/deploy steps against it.
* Trace the Test cases that were setup from the java code base and get hints on the unit tests performed to identify functional behavior that may lead to possible causes of the crash issue.



4.    Update on the Layer.com staging push service certificate
* It was identified via Layer’s Push configuration that these certificates were expired from last year’s October 2016. 
* By the time this certificate was updated and a new “Debugged” version of the tgw-dev-market java code was deployed
* First to test the hypothesis about the push certificate a possible cause of the issue, tested the new version from letter b (above) with the same test notification storm code with image content, logged user with 100 or more friends and below is the latency from EB instance monitoring (average latency) graph.  The latency reaches all the way to 60 but then, it goes back down. It's not stuck all the way to 60 like the ones before. It's temporary only for a certain time but recovers.
![Latency](images/Latency.png)
* So after this finding, tried to simulate the crash with the following steps:
* * 	Post content with big image.
* *	User who posted has at least 100 friends.
* *	Interleaved posting between 3 different browsers up.
* *	This is done approximately up to 30 attempts as total between the 3 browsers
![Latency3](images/Latency3.png)


* Server crashed but with new set of error logs are showing. This means that the previous version of error logs shown from the screenshots above can no longer be reproduced. Please see comparison.
![sroutlog1](images/stdouterrlog1.png)
![sroutlog2](images/stdouterrlog2.png)
* The DEV I assume no longer shows the errors mentioned or shown above simple because the push certificates were updates so certainly the application (market) and the layer (push service) can get the notifications through.  But even with the certificate updated, performing the same stress test above still somehow get some crashing issue on the tgw-dev-market EB instance. 
* Both servers CRASHED or is CRASHING. It did not matter whether a VALID or INVALID certificate was used in the first place. Pushed or not pushed had little or nothing to do with the reason for the cause. It could be the operators and how the design was implemented in the first place.




5. Action Items continued...