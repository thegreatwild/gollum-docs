# Copied from TGW Drive, somewhat outdated info
__Browse TGW drive for other docs__

## Google Apps
https://admin.google.com/thegreatwild.com/AdminHome
* Admins are Kristin, Carlo and Micke
* Users
* New users are added in Users —> + sign —> Add user
* I’ve added users with [firstname]@thegreatwild.com and then added the alias [firstname].[lastname]@thegreatwild.com
* Users → Go into specific user → Account → Add an alias
* New users needs to be added to groups manually (except for all@thegreatwild.com), this is done under Groups
* Groups —> Click on group —> Manage users in [group name] —> Add user
* Groups
* Create group and add users
* Access permissions (Access settings)
* Permissions —> Basic permissions —> Join —> Public
* E-mail routing
* For unknown e-mail mailbox account messages the e-mail is routed to the catch-all address set to carlo@thegreatwild.com 
* https://support.google.com/a/answer/33962?hl=en 
* Good to know
* Wrike and Insightly has their own app, to install them find them in the Marketplace (https://admin.google.com/thegreatwild.com/AdminHome#AppsList:ca=SERVICE&serviceType=MARKETPLACE )

## Lastpass
* In Lastpass plugin you can find the Admin console option second from the bottom; Kristin, Micke and Carlo are currently admins
* In the dashboard (Home —> Dashboard) you can purchase new licenses and show receipts
* Licenses are bought one at a time and are valid until 2016-12-21 even if it is assigned or not, licenses must be manually renewed
* Add users via Users —> Create new user —> Invite people by adding their e-mail addresses
* If no licenses are free you need to first purchase more before 
* Each user needs to install the Lastpass plugin on their browser to use it
* Add users to User groups for access to Shared folders
* Currently we have two user groups:
* TGW - General —> Access to folder "The Great Wild - General"
* TGW - Tech —> Access to folders "The Great Wild - Tech" and "The Great Wild - General"

## Jira
* Currently all users have all possible permissions - anyone can add new users and create new projects
* Add new user: Jira administration (cog wheel top right corner) —> User management —> [Confirm log in] —> Create user —> Fill in details and don’t forget to tick “Send notification e-mail”
* New users are automatically assigned to the Group "jira-users", for each new user click on corresponding Groups button —> In the top field write “jira-administrators” and “jira-developers” —> Click Join selected groups

## Slack
https://thegreatwild.slack.com/admin 
* Primary owner: Johan, admin: Kristin
* Add users
* Manage your team → Invite New Members → Full Members → Add e-mail address and name → Invite
* Remove users
* Manage your team → Click on user status for person → Disable account

## Insightly
https://c7jvq846.insight.ly/home
* For use, see the document “Sales process outfitters and introduction to Insightly” on https://drive.google.com/drive/u/0/folders/0B1O3RvCLuvnOUjQ4OTlWSDJSZ0E 
* Account owner: Carlo, admins: Carlo and Kristin

## Zendesk
https://thegreatwild.zendesk.com/
* Customer support software, trial period has expired and to add users (agents and admins) upgrade is necessary before any action can be taken for the account
* Kristin is admin and Johan is agent (currently not possible to upgrade Johan to admin, see above)
* When the time comes (when we get a lot of customer support cases again) we can activate it 
* E-mails to support@thegreatwild.com are forwarded to support@thegreatwild.zendesk.com (already set up in Google Apps)

## Access to office Gbg
* Send e-mail to reception@chalmersfastigheter.se
* If person have key card, ask for access to the building on that card number
* If person don’t have key card, add the persons name and personal number to the request for access. A new card will have to be picked in the reception in Villan.

## Checklist employee onboarding
* Accesses
* Google Apps
* Slack
* Lastpass
* Folder on Drive
* Jira
* Key / access card

## Checklist employee offboarding
* Accesses
* Google Apps
* Slack
* Lastpass
* Drive (if old employee, before Google Apps)
* Jira
* Key / access card
* Receipts and other documents
