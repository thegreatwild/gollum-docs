# Android Testing Environment Setup
A guide to set up the work environment for Android testing.

## Run Android SDK Manager
1. Download and install the latest:
 * Android SDK Tools
 * Android SDK Platform-tools
 * Android SDK Build-tools
 * (Optional) Intel x86 Emulator Accelerator (HAXM installer)
2. Download and install the:
 * SDK Platform
 * System Image of your choice for: 

        • Android 6.0 (API 23)

        • Android 5.1.1 (API 22)

        • Android 4.4.2 (API 19)

## Run Android Virtual Device (AVD) Manager:
1. Create 3 devices for Android 4, 5 and 6

2. We used these settings where AVD Name, Target and CPU/ABI are the only fields that changes according to what you downloaded.

![AVD Screenshot 1](images/avd-1.png)

Here’s what we have currently:

![AVD Screenshot 2](images/avd-2.png)

## Run Android Emulator and Start Testing the APK:
1. Simply select the AVD item you want to run and click _Start_...

2. The emulator should load
* If there are any errors regarding HAXM, You need to disable a Windows feature
          	
        • Control Panel → Programs → “Turns Windows features on or off” (under “Programs and Features”) → Find “Hyper-V” → Uncheck → Reboot

        • Turn on/Enable the Virtualization in the BIOS settings. 

3. Install the APK file to the emulator
* Run the command: `adb -e install -r yourapp.apk`

4. Start testing