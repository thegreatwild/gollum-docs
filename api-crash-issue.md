## Summary of Errors/Exceptions from Python Api Backend (**March 19 05:54:38 to March 21 08:48:13**)
Note: ('xx') next to specific errors/exceptions pertain to the number of occurrences from logs extracted in a given period

## **Categories from '/opt/python/log' (python code logs)**

### **Http Errors (app.log)**
* **(WARNING) Error code: 405 Message: HTTP 405: Method Not Allowed (886x)** 
* * python server is telling that the method is not allowed for the URL you are trying to call. In this case, it is from the **http://www.uptimerobot.com/** that is trying to get to api.thegreatwild.com

### **Access Errors or Warning (app.log)**
* **(WARNING) Couldn't get current session user - (1715x)**
* * When route /auth is reached, the BaseHandler will try to get the current user from the session object and if it can't return the current user instance, will display this error.

* **(INFO) Invalid session token xxxxxxx... (357x)**
* * This error is propagated when the python code tries to extract from the request header object an 'Authentication' key (the token object). SessionHandler tries to fetch the session object using the token from the RedisMemcache object. If session object was not found OR session token is expired, the error is thrown.

* **Error code: 1000 Message: Not allowed (57x)**
* * Caused by the decorator object 'authenticated' to require user has to be a 'logged' user. If current user is not found, it will raise the Unauthorized error status 'Not Allowed' 

* **Error code: 1014 Message: Wrong password (52x)**
* * User attempts login and the EmailAuthHandler tries to find the user from database. If user was found BUT password in invalid, it raises an Unauthorized HTTPError (WRONG_PASSWORD)

* **Error code: 1015 Message: Email does not exist (19x)**
* * User attempts login and the EmailAuthHandler tries to find the user from database. If not found, it raises an Unauthorized HTTPError (EMAIL_DOES_NOT_EXIST)

* **Couldn't update like_count for report item #xxxx (3x)**
* * When application accesses /like/report/id endpoint, the LikeReportHandler calls the update_like_count method to update the object referenced by the id param.  If a resultwrapper object is not returned when the query executes, it fires this warning message


### **Exceptions (app.log)**
* **Uncaught exception POST /auth/email (26x)**
* * File "/opt/python/run/venv/lib64/python2.7/site-packages/bcrypt/__init__.py", line 148, in hashpw
    raise ValueError("Invalid salt")
ValueError: Invalid salt
* * More to follow


### **Email Errors (error.log)**
 
* **SendGrid: Failed to send mail (11x)**
* * details coming
* **Failed email to user on join (11x)**
* * More to follow

### **Exceptions (app.log)**

* **Uncaught exception POST /auth/email (8x)**
* * File "/opt/python/current/app/src/utils/password.py", line 13, in __set_secret
    self.secret = str(secret).encode('utf-8') UnicodeEncodeError: 'ascii' codec can't encode character u'\xe4' in position 0: ordinal not in range(128)
* * Error code: 500 
    Message: 'ascii' codec can't encode character u'\xe4' in position 0: ordinal not in range(128)
* * More to follow

* **Uncaught exception PUT /change-password/email (12x)**
* * File "/opt/python/current/app/src/handlers/change_password.py", line 27, in put
    raise RequestError(errno.WRONG_PASSWORD, "Wrong Password")
* * Error is raised from the ChangePasswordHandler when application attempts to update the user's current password but the old password hash is not a valid one.

* **Uncaught exception POST /auth/email (18x)**
* * File "/opt/python/run/venv/lib64/python2.7/site-packages/bcrypt/__init__.py", line 148, in hashpw
    raise ValueError("Invalid salt")
ValueError: Invalid salt
* * Error code: 500 Message: Invalid salt
* * This exception will also be returned if anything goes wrong while hashing a password from the bcrypt source for hashpw(). If for some reason it is unable to invoke the bcrypt lib at all, it will return the Invalid Sald error.

* **Uncaught exception POST /register/email (3x)** 
* *File "/opt/python/current/app/src/handlers/register.py", line 78, in post
    raise RequestError(errno.ACCOUNT_ALREADY_REGISTERED, "The email is already registered")
* * More to follow
