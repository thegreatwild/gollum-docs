# App features

## Landing screen
* Change language
* Forgot password

### Registration
* Email
  1. Fill in first and last name, email and password
  2. Confirm details
* Facebook
* If a user comes via an invite, a hash (linking together inviter->invited in our db) is included as a get param 

### Login
* Email
* Facebook

## News
On this screen you can scroll through your news feed. You will see trophies, observations and statuses of other users, including likes (number of likes and the names of any friends who have liked) and comments they have.

* Post Trophy
* Post Observation
* Post Status (editable within 60 minutes after posting it)
* Post embedded content (using [Embedly](http://embed.ly/))
* Like/unlike trophy/observation/status
* Double tap on post image to like
* Comment (and remove your comments) on trophy/observation/status
* Report trophy/observation/status
* View profile of other users

## Post trophy
1. Take or upload photo (mandatory)
2. Choose type of game
3. Set date, number and category (parent/children/etc) of the game, and write a description (optional)
4. Set map location

## Post observation
1. Choose type of game
2. Take or upload photo (optional), set date, number and category (parent/children/etc) of the game, and write a description (optional)
3. Set map location

## Post status
1. Write the status, take or upload photo (optional)

## My hunts
Here you will see a list of hunts you have created and your current bookings.

* Booking requests will be listed at the top of the page, then drafts (which you can edit), and then upcoming hunts (grouped by month)
* If you have unanswered booking requests, a badge will be displayed
* For each hunt, you will see: status of booking request (confirmed/pending/denied), basic info the hunt. You can click on the hunt to view the detailed info (see below).
* If you press "Bookings" on a hunt, you will see a list of pending and accepted bookings for that hunt
* Invite to hunt (see below)
* Handle booking requests (see below)

## Create hunt
You can create two kinds of hunts; private or public.

For a private hunt, required fields are:
* Title
* Date
* Contact Name

For a public hunt, required fields are:
* Title
* Image
* Date
* Hunt Type
* Description
* Contact Name
* Contact Phone
* Hunt Address
* Hunt Zip

## Booking request widget
Can be seen if a hunt has a pending booking request.
This is where you reply to someone's booking request to one of your hunts.
* You will see the name of the person, number of sent booking requests (=number of seats), accepted bookings
* You can reply to the booking request (accept/deny)

## Find hunt
* View nearby hunts on the map (pins are based on the hunting team's position. Click one to get an overview of the team's hunts)
* View hunts in list view (sorted by date)
* In both views, you can search by description and also filter by price range, date range and allowed game
* In both views, you can also do a free text search using the input box in the top bar
* You can also invite to hunt from here (see above)

## View hunt
* View detailed hunt information
* Show on map
* Request to book (only for hunts you have not created yourself)
* Share hunt (copy link or share to Facebok)
* For hunts you have created and also for hunts you have been accepted to, you will also see assembly point and contact person.
* You can also edit or delete the hunt.
* You can also send a message to the host of the hunt.

## Request to book hunt
1. Fill in number of seats, email and message (optional)
2. Send booking request

## Activity
Here you will see any new notifications you have received:
* Booking requests (press it to go to "handle booking request")
* When a friend or a member of one of your hunting teams has posted a status/trophy/observation
* When someone has liked or commented on your status/trophy/observation
* When someone comments on a status/trophy/observation you have commented before
* When you are accepted or denied to a booking
* When someone sends a friend request to you
* When someone accepts your friend request
* When one of your Facebook friends joins TGW (you also become friends automatically)

## Profile
* View stats for your observations and trophies of different kind of game.
* View recommends
* View your friend list
* View the hunting teams you are a member of (accessible from the menu bar instead in desktop mode)
* View your hunting grounds(=trophies and observations on the map) (accessible from the menu bar instead in desktop mode)
* View your image gallery (trophies and observations)
* Add photos of observations/trophies to your image gallery (only if your gallery is empty)
* Access settings menu
* View your inbox (todo)

## View profile of other users
* See statistics of user
* See recommendation count
* Add as friend
* Recommend user
* View image gallery (trophies/observations)
* Send message
* See friends of user

## Hunting teams
* View the hunting teams you are a member of
* Create a new hunting team

## View hunting team
View a single hunting team you are a member of
* View team image, name, members, posts of hunting team
* Can see the hunting team's assembly point on a map
* Post comments to your hunting team
* Edit Team
* Delete team

## Create hunting team
1. Upload a team image (optional)
2. Enter team name
3. Place team on map (optional)
4. Invite members (optional) 

## Friend list
* Invite contacts to app (mobile only)
* View a list of your friends
* Search (by name) for new friends to add
* Handle pending friend requests

## Settings
* Change profile picture
* Change language
* Change phone number
* Invite friends to app (via email)
* Contact support (via email)
* View user agreements
* Log out
* See build version and current environment

Admin only:
* Impersonate user
* Change environment (production/development)
