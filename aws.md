# Amazon Web Services (AWS)
The following AWS services are used.

* **Elastic Beanstalk**: Deployment tool.
   * tgw-dev-api / tgw-prod: Python backend
   * tgw-dev-market / tgw-prod-market : Java backend
   * configuration -> software configuration : set system variables
   * upload and deploy : zip file including ROOT.war, Dockerfile and Dockerrun.aws.json

* **EC2**: Runs the instances of the backend servers. Load balanced so that new instances will be created automatically when necessary.

* **S3**: Hosts thegreatwild.com, app.thegreatwild.com (updated through the deploy.py script in the frontend project), test-app.thegreatwild.com (here you can test new version of the web app before deploying them for real), the tgw image server and other static data. 

* **CloudFront**: Caches data retrieved through calls to S3.

* **RDS**: Relational Database Service.
   * tgw-dev-api / tgw-prod
   * security groups : set up ip access etc.
   * tgw-test can be used for user testing new versions of the database.

* **DynamoDB**: 
   * translations
   * features: can be used to control feature toggling in the app from the backend.
   * users: stores ios/android id's for each user (used for push notifications).

* **ElastiCache**: redis cache for session id's (so that a user is not logged out after a database update).

* **IAM**: Manage AWS access.