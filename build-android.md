# How to build and deploy for Android/Google Play

1. Run `./deploy.py`
2. Run `cordova build android --release` 
3. Verify the version number at second row in platforms/android/AndroidManifest.xml
   * If it is the correct one, proceed 
   * if it is wrong redeploy. If you have redeployed and this is still wrong something is broken with the version replacement script
4. Sign the apk. 
   * Download keystore from drive if it isn’t already added in the project and save in the project root (name it com.thegreatwild.keystore)
   * run `jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore com.thegreatwild.keystore platforms/android/build/outputs/apk/android-release-unsigned.apk tgw`
   * Use passphrase. It can be found as an image in tech/certifikate in the TGW drive. 
5. Verify the signing:
   * `jarsigner -verify platforms/android/build/outputs/apk/android-release-unsigned.apk`
   * Output should say “jar verified” Warnings about certificate chain can be ignored. Certificate is valid until year 4752
6. zipalign the apk
   * `zipalign -f -v 4 platforms/android/build/outputs/apk/android-release-unsigned.apk com.thegreatwild.apk`
7. Go to https://play.google.com/apps/publish and upload file com.thegreatwild.apk that is in the project root 



