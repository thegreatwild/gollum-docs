# How to build and deploy the backend

*Detta skall göras vid en tidpunkt som folk helst inte använder appen.*

1. Ersätt dev-databasen mot prod-databasen (to be safe).
   1. Om det är kritiska .sql scripts som kan förstöra mycket, gör en databas dump av prod först.
   2. Kör .sql filerna i /db mappen (bör ligga i market) på dev-databasen.
   3. Verifiera att saker och ting fungerar i appen.
   4. Om det fungerade bra, kör sql-scripten på prod-databasen.
2. Deploy backends till EC2 maskinerna, (tgw-prod och tgw-market) med senaste backend **via Elastic Beanstalk**.

---

Ifall något mot förmodan skulle gå fel så har appen inbyggt att:
* Ifall backenden skickas error #1013 (OTHER_INFO), eller error #1014 (OTHER_ERROR) med tillhörande data, så kommer dessa meddelanden visas som ett flash på användarens mobiltelefon. Kan användas för att säga att en uppdatering är på väg.

---

Ifall produktionsmiljön går sönder helt:
* Appen som är i produktion är kopplad till DNS: api.merithon.com, eftersom test miljön nu bör vara exakt kopia av databasen med en fungerande backend så är det bara att byta dns via CityNetwork.

---

