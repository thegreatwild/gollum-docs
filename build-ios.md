# Deploy instructions for iOS

Make sure that:

1. The path to the project folder (tgw-app) is on `<user>/dev/tgw-app`
2. `npm install` was already done on tgw-app

### 1. Run Deploy Script

cd into the project folder and run `python deploy.py`

![XCode-Build-Button](images/deploy-py.png)

When deployment is Done, without errors, Scroll up and make sure you don't have this error:

![XCode-Build-Button](images/deploy-error.png)

If you have this, you have to re-run the deploy.py.

### 2. Open XCode Project File and Check The Following Configurations

* ### Setting up Certificates

**1. Adding Apple ID**

Add an your Apple ID that is a member of **The Great Wild AB**. After adding the Apple ID, set the Team to **The Great Wild AB**.

![Add-Account](images/Add-Account.png)

**2. Adding Distribution Certificate**

Go to [developer.apple.com](https://developer.apple.com) and under **Certificates, Identifiers & Profiles**, download the Distribution Certificate. Double click the file to add it on the Keychain.

![Distribution-Certificate](images/Distribution-Certificate.png)

You might get an error when you  try to Archive the project. You will have to uncheck **Automatically manage setting** and manually select the Provisioning Profile that has the **Distribution** Signing Certificate (Shown on "2. Build Settings" under Deploy Settings). 

Dealing with development type has a similar workflow from above but without the need to uncheck **Automatically manage setting**.

* ### Deploy Settings

1. **General**
      * Display Name : `${PRODUCT_NAME}`
      * Bundle Identifier : `com.thegreatwild.app`
      * Version & Build Number
      * Team : `The Great Wild AB`
      * (For Distribution) Uncheck Automatically manage signing and provide the appropriate Provisioning Profile for Distribution (This is just conditional if you get errors regarding this matter)
      * Deployment Target & Devices
2. **Build settings**
      * Build Options > Enable Bitcode : `No`
      * Signing > Code signing must point to appropriate Distribution Certificate (iPhone Distribution: The Great Wild AB...) if you build a production release. Not iOS Developer because if you build dev version, it works on either way.
![Code-Signing-Identity](images/Code-Signing-Identity.png)
3. **Capabilities**
      * Turn ON Push Notificiations
4. Make sure that the "active scheme" is "The Great Wild" and "iOS Device" (or the device connected)
      ![XCode-Build-Button](images/XCode-Build-Button.png)

### 3. Conduct End-to-end Test on an Actual iOS Device
   * Check icons, names, configuration markers.
   * Make sure the content is from the correct database
   * Follow through the test cases from the End-to-end test guide/document

### 4. Run Product > Archive

### 5. For App Store
   1. Make sure that the environment variable is set to "prod" (in environment.js) and that the version number matches everywhere (/config.xml, /deploy.version and optional.js)
   2. Click "Upload to the App Store", provided that iTunes Connect account is logged in Xcode.
   3. Otherwise, choose Export and Save the App Store Deployment -> Start Application Loader and upload to iTunes Connect
   4. Log into iTunes Connect, update information for the next version
      * version number
      * Changelog
      * Screenshots / Icons
      * Switch to binary release
   5. Submitting to review the "manual release"


### För Testfairy
   0. (Kolla så att Provisioning Profile är satt till Distribution på alla i Build Settings)
   1. In XCode -> Product -> Archive
   2. Välj Export och Ad Hoc deploymen i “Archive Information”-menyn till höger
   3. Välj rätt Provisioning Profile om du inte gjorde det i 2|b|ii
   4. Spara .ipa-filen där du hittar den
   5. Gå till testfairy.com och logga in.
   6. Press Apps, Upload ios ipa
   7. upload .ipa
   8. Skriv changelog
   9. Ge endast dig själv permission för builden och godkänn.
   10. Tanka hem appen från din pryl som om du vore en testare 
   11. Om allt fungerar som du förväntar dig, gå till senaste builden på Testfairy och ge permission till alla som skall testa
