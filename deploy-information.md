# Deployment information

## Test
Innan en release av appen kan göras så bör ny funktionalitet testas i följande enheter/versioner:  

* Android
  * Version ~4
  * Version ~5
  * Version ~6
  * Version ~7
  * Version ~8

* iOS
  * Version ~8
  * Version ~9
  * Version ~10
  * Version ~11

* Web
  * Safari
  * Chrome
  * Firefox

## Preparations
1. Hämta senaste koden
2. Konfigurera webapp
 * Välj rätt servermiljö, test eller live, i config.optional.js
 * Välj "prod" som default-miljö i TGW-app i filen environment.js
3. Kontrollera att resurser är uppdaterade (ikoner och splashscreens)

## Git Tags
1. Sätt en git tag på den commit som har släppts och pusha upp tags till release-branchen/master (t.ex.)
   * $ git tag -a v13 -m “New version 13 with messaging functionality” 
   * $ git push origin --tags

## Platform specific deployment instructions
* [Build and deploy the web app](deploy-web)
* **(Outdated due to new architecture)** - [Build and deploy for iOS](build-ios)
* **(Outdated due to new architecture)** - [Build and deploy for Android](build-android)
* **(Scrapped and not being used anymore)** - [Build and deploy the backend](build-backend)
* **(Scrapped and not being used anymore)** - [Deploy Java backed](deploy-java)
* **(Scrapped and not being used anymore)** - [Deploy Python backed](deploy-python)

## Deploy Script Workflow Revision
Due to the meta tags not being retrieved correctly by Facebook and other bot crawlers when the Dynamic Meta Tags are populated by AngularJS alone and is hosted on S3, there was a need to host it on a proper web server which the AWS Elastic Beanstalk provides.

The reason why populating Dynamic Meta Tags using AngularJS doesn't work because Facebook and some bot crawlers DO NOT execute JavaScript files so the templates won't be interpolated with the correct values. The host will then return the plain template and the bot crawlers will try to parse the meta tags but there are no string values so the bot crawler fails to retrieve the meta tags.

S3 doesn't have a middleware as opposed to AWS Elastic Beanstalk where we have the Node Express to act as the middleware. So what the Node Express does are:

1. Intercepts the HTTP requests
2. Based on the URL requested, Node Express retrieves the correct meta tag values.
3. Node Express then renders the meta tag values to "index.ejs" and is then returned to the requester (bot crawler or end user). This way, the meta tags are already populated when it's returned to the requester as opposed to via Angular where the scripts are needed to be loaded first for interpolation to work.

**Note:** There are 2 index files (_index.ejs_ and _index.html_) to maintain.

* **index.ejs** for Web-App (Found only in the **root/www** folder)
* **index.html** for Mobile Apps

### Diagram of the revised workflow:
![Deployment-Workflow-Revision-Diagram](images/Deployment-Workflow-Revision-Diagram.svg)

Diagram was made using draw.io, the XML document can be found in the images folder.