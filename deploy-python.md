- `vagrant up`   
- `vagrant ssh` (password: vagrant)   
- `eb init`  
- These values should be default:   
    - AWS Region: eu-west-1   
    - AWS Elastic Beanstalk Application: tgw   
    - AWS Elastic Beanstalk Environment: tgw-dev-api   
- Follow the instructions on the screen to provide AWS Access Key and AWS Secret Key if there wasn't a aws_credential_file supplied during provisioning (these credentials are generated in AWS -> IAM Console -> Users)   
- `eb deploy`