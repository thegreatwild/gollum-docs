# Emoji Issue - v1.0.0

### A. Try Python Versions other than current 2.7 Python Version
Try with Python 3.6 to see if it works and verify if emoji issue is isolated with Python 2.7<br>
[Can't install mysql-python version 1.2.5 in Windows](http://stackoverflow.com/questions/37092125/cant-install-mysql-python-version-1-2-5-in-windows)<br>
Manually install the dependencies using `pip`

* Use `python36 -m pip install xxxxx`
* Manual packages needed to be install via CLI pip command
   * Mysql-python==1.2.5
   * Cffi==0.8.6
   * Cryptography==0.6.1
   * Wsgiref==0.1.2
   * Bcrypt==1.1.1
* After doing manual installation via ‘pip’, will need to install the VS C++ dependency stack

### B. Encoding [PEP 263 -- Defining Python Source Code Encodings](https://www.python.org/dev/peps/pep-0263/)

### C. MysqlDb-python 1.2.5 does not support utf8mb character set

### D. [Issue 11742: MySQLdb-python 1.2.4 does not support utf8mb character set](https://code.google.com/p/googleappengine/issues/detail?id=11742)

### E. How to encode utf8mb4 in python

### F. [How to encode (utf8mb4) in Python](http://stackoverflow.com/questions/26532722/how-to-encode-utf8mb4-in-python)

### G. Can’t Initialize to utf8mb4

### H. [“Can't initialize character set utf8mb4” with Windows mysql-python](http://stackoverflow.com/questions/38813689/cant-initialize-character-set-utf8mb4-with-windows-mysql-python)

### I. Peewee MysqlDatabase API

### J. [peewee](http://docs.peewee-orm.com/en/latest/peewee/database.html?highlight=mysqldatabase)

### K. [MySQLdb User's Guide: Some _mysql examples](http://mysql-python.sourceforge.net/MySQLdb.html#some-mysql-examples)

### L. I am opening the mysql connection with `charset='utf8mb4’`
   **1.** This link shows in the discussion concluding ‘python-mysql 1.2.5’ only supports up to 5.5 (Major Blow)<br>
   **2.** Error on build:<br>
       
       sudo mount -t vboxsf Shared /home/zeferinix/Documents/Shared
       File "C:\Python27\lib\site-packages\MySQLdb\connections.py", line 312, in set_character_set
               super(Connection, self).set_character_set(charset)
                 OperationalError: (2019, "Can't initialize character set utf8mb4 (path: C:\\mysql\\\\share\\charsets\\)")`

### H. Question Marks in Mysql

### I. [mysql server replaces supplementary Unicode characters with a question mark (?)](http://stackoverflow.com/questions/38153123/mysql-server-replaces-supplementary-unicode-characters-with-a-question-mark)

### J. [Django OperationalError 2019 Can't initialize character set utf8mb4](http://stackoverflow.com/questions/18796168/django-operationalerror-2019-cant-initialize-character-set-utf8mb4)