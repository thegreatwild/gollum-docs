# Linux Virtual Machine Environment
This documentation is for Windows users who need to setup a Linux VM Guest to be able to deploy the _tgw-app_ (Front-end) for Android and Web platforms.

## Requirements
1. VirtualBox
2. Lubuntu Image ISO 64-bit (Cordova builds doesn’t work with 32bit in Linux)
3. JDK 64-bit for Linux
4. Android Studio for Linux
5. Clone [Facebook SDK](https://github.com/facebook/facebook-android-sdk )

## Guide to setting up VirtualBox and Lubuntu:
* [Setting up a Lubuntu virtual machine with virtual box [and get guest additions working]](https://dwheelerau.com/2014/01/25/setting-up-a-lubuntu-virtual-machine-with-virtual-box/ )

## Guide to setting up a Shared Folder between Host and Guest:
* [Virtualbox Tutorial: How to share folders from Windows to Ubuntu](https://www.youtube.com/watch?v=ddExu55cJOI)
* Syntax to run in Linux:
`$ sudo mount -t vboxsf <FOLDER_NAME_FROM_HOST> <FOLDER_PATH_TO_GUEST>`

## Install tgw-app (Front-end) Dependencies:
* `sudo apt install git npm nodejs-legacy ruby ruby-sass ant`
* `sudo npm install -g bower grunt-cli cordova plugman`

## Setup Java JDK:
1. Download the latest Java JDK, we used the tar.gz 64-bit
2. Unpack it somewhere
3. Remember the PATH where you unpacked it for setting up the Environment Variables later

## Setup Android Studio:
1. Download Android Studio for Linux
2. Go to the Android Studio folder
3. Go to ‘bin’ folder inside the Android Studio folder
4. Run ‘studio.sh’ to start setup
5. Install
6. Remember the PATH where it will be installed for setting up Environment Variables later
7. This will take some time (Download the latest SDK, etc.) so go setup something else while this is installing.

## Setup Android SDK Platforms:
1. Type ‘android’ in bash and confirm if the SDK manager appears.
2. If not, this means your Android Path was not correctly setup, you haven’t setup the Android Path yet, or you don’t have it installed. Else, proceed.
3. Download the target SDK of the project and make sure to check the agreement license.

## Setup Environment Variables:
1. Go to Home folder
2. Press CTRL+H simultaneously
3. Open .bashrc
4. Paste the values below to the end of the .bashrc file and adjust the values accordingly to your setup (Paths)

       export JAVA_HOME=/home/zeferinix/Documents/Java/jdk1.8.0_112
       export PATH=$PATH:$JAVA_HOME:$JAVA_HOME/bin
       
       export ANDROID_HOME=/home/zeferinix/Documents/Android/Sdk
       export PATH=$PATH:$ANDROID_HOME/tools
       
       export FACEBOOK_HOME=/home/zeferinix/Documents/facebook-android-sdk
       export PATH=$PATH:$FACEBOOK_HOME
       
       sudo mount -t vboxsf Shared /home/zeferinix/Documents/Shared

## Finally:
* You should be able to run and deploy for Android and Web at this point by running the _deploy.py_ script in the tgw-app repo. Cheers!

<font color="#ea4535" size="6px"><strong>NOTE</strong></font>
* You may need to restart after setting up all of these.
* Every time you go to the terminal, you'll be asked for a password. This is normal due to the line `sudo mount -t vboxsf <param1> <param2>`

<font color="#ea4535" size="6px"><strong>TO DO</strong></font>
* <font color="#ea4535"><strong>Look for another approach so that this is automatically done without user input.</strong></font>