# MixPanel Events

MixPanel automatically adds some basic info to all events, such as user id, timestamp, device etc.

We also add the following custom properties to different kinds of events:

## Messaging: Start conversation
Success  
On success: conversationId, recipientId  
On error: full error object

## Messaging: Message sent
Success  
On success: conversationId, recipientId, messageBodyLength, messageId  
On error: full error object  

## Messaging: Message received
conversationId, messageId, senderId

## Messaging: Message read
Success   
On success: messageId   
On error: full error object  

## Conversation List View: Open

## Conversation View: Open
conversationId

## From notification: open conversation view

## From hunt preview: Open conversation view

## From user profile: Open conversation view

## Find hunts: Click on hunt
HuntId 

## Hunt view: Click on book
HuntId

## Article: Comment created
Success  
On success: commentId, articleId  
On error: full error message

## Article: Comment deleted 
Success    
On success: commentId    
On error: full error message

## Like
articleId

## Unlike
articleId

## Menu: Feed
## Menu: My hunts
## Menu: Finds hunts
## Menu: Notifications
## Menu: Profile

## Team Created

## Status Uploaded

## Added User(s) To Team

## Team Deleted

## Deleted User From Team

## Team Updated

## Click on Terms & Conditions

## Add Credit Card

## Create Hunt: New Hunt

## Create Hunt: Published

## Create Hunt: Saved as Draft