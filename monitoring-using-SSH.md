### Configuring to Linux Instance using Putty
1. Install Putty (http://www.chiark.greenend.org.uk/~sgtatham/putty/)
2. Get the public DNS name of the instance from Amazon EC2 Console. Check teh Public DNS (PSv4) column.
3. Locate or generate the private key (.pem) for the key pair specified when creating the instance.
To get the key pair, it's either given or you'd have to generate a new key pair. To create and download a new key pair, on AWS EC2 Console, Go to Network & Security > Key Pairs. As soon as a new key pair is generated, it will automatically download a *.pem file. NOTE: Please keep this .pem file somewhere safe as this will only be downloaded ONCE.
4. To authorize SSH access to EB instance, go to EB Console and on Configuration Tab, select Instances and update the dropdown for Key Pair to the newly created key pair from previous step. Note that this will restart the EB instance to apply the changes so timing is critical as to when this should be performed.

For further details of the steps above, please check this link
http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html


### Monitoring TGW Market
1. For monitoring the java code logs, in SSH, go to the directory /var/log/eb-docker/containers/eb-current-app
2. Type 'tail -f <containerid>-stdouterr.log'
3. For monitoring the nginx access log, go to directory /var/log/nginx
4. Type 'tail -f access.log'
5. For monitoring the nginx error log, go to directory /var/log/nginx
6. Type 'tail -f error.log'

### Monitoring TGW API
1. For monitoring the python code logs, in SSH, go to directory /opt/python/log
2. Type 'tail -f app.log'
3. For monitoring python error log, go to directory /opt/python/log
4. Type 'tail -f error.log'