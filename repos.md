CTO as of writing decided to move to bitbucket and rewrite the server from scratch. Old repo info are still below for archive purposes.

## [tgw-app](tgw-app)

* Web app front-end
* Now only used for deployment to web and this web app is loaded into the app container
* https://bitbucket.org/thegreatwild/tgw-app
* Production: https://webapp.thegreatwild.com
* Develop: http://dev-app-eb.thegreatwild.com

Note: If you are to add a cordova plugin, add it to tgw-app-container and NOT on this repo.

## tgw-app-container

* Mobile app front-end
* Used to load the front-end web app
* https://bitbucket.org/thegreatwild/tgw-app-container

## tgw-graphql

* Node GraphQL server
* https://bitbucket.org/mattiasmak/tgw-graphql


<hr>
<hr>
<hr>

At http://stash.thegreatwild.com/projects you will find all the repos of the TGW projects (app and webpage). The different repos are described briefly below.

## email-templates
Email templates

## [tgw-app](tgw-app)
App frontend
Used for ios/android/

http://app.thegreatwild.com
http://dev-app.thegreatwild.com
http://test-app.thegreatwild.com

## tgw-back
Python backend (legacy)

## tgw-drupal
It is dead

## [tgw-market](tgw-market)
Java backend

## tgw-web
Webpage (landing page, www.thegreatwild.com)