# Shared Public Hunt Page
A documentation on the endpoint and involved server code regarding the publicly shared hunt page

# Example
Example used for this URL can be found [here](https://app.thegreatwild.com/#/find-ads/preview/101).

![Ads/101](images/2017-01-06 (TGW) 1.png)


## The endpoint for shared public hunts is in the format:

* https://market.thegreatwild.com/public/hunts/:id


# Server Code

![Shared Hunt Ads Public](images/2017-01-06 (TGW) 2.png)

1. (RED) Creates an object based on the Front-end request ID
2. (BLUE) The object is then used to look for the requested hunt ad based on the request ID. Once the ad has been found, the ad object is returned
3. (GREEN) This object is then wrapped and returned back to the Front-end.

* (PURPLE) Is the ad model
* (YELLOW) the get() method is a wrapper for Spark