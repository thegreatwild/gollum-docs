# Android 4

## Login

1. Email login - with correct credentials.
2. Email login - with wrong credentials.
3. Login with Facebook.
4. Go to settings view and logout.

## Feed

1. Select a feed item that has no likes and like it.
2. Unlike a feed item.
3. Add a comment to any feed item.
4. Add a comment to another feed item and remove it.
5. Report a user but click cancel button.
6. Scroll down the list to fetch more feed items.

## Tab menu

1. Should be able to click on all tabs and ensure content and that tab menu is highlighted.
2. Press log button and close it.
3. Press log button and ensure each use case works.

## Find Hunts

1. Click on “Find Hunts” tab menu button and ensure content. Empty is a valid option.
2. Toggle between map and list view.
3. Select the first hunt in the list. 
4. Select the last hunt in the list.
5. Select a any hunt from map view.
6. Create new hunt and publish it.
7. Book a hunt with one seat and type in a message to the seller and go to payment. 
8. Book a hunt with two seats and type in a message to the seller and go to payment.
9. Book a hunt with one seat as a new customer.
10. Book a hunt with one seat as an existing customer, e.g. select the stored credit card.
11. Book a hunt with wrong credit card information (see https://stripe.com/docs/testing)


## Misc

1. Toggle language from settings view.
2. Go to your profile and click on “my hunts” button.
3. Navigate in the app. E.g. navigate into different use cases and press back button, to go back to original location.
 
## Messaging

1. Send a private message to a friend.
2. Receive a private message to a friend.
3. Live chat (the user doesn’t need to refresh to see a new message from the conversation).
4. <font color="#ea4535"><strong>Pressing the push notification received from a private message redirects you to your conversation with the user who sent the private message.</strong> (Needs testing on actual device that runs Android 4. Push Notifications don't work in AVD emulators)</font>

# Android 5

## Login

1. Email login - with correct credentials.
2. Email login - with wrong credentials.
3. Login with Facebook.
4. Go to settings view and logout.

## Feed

1. Select a feed item that has no likes and like it. 
2. Unlike a feed item.
3. Add a comment to any feed item.
4. Add a comment to another feed item and remove it.
5. Report a user but click cancel button.
6. Scroll down the list to fetch more feed items.

## Tab menu

1. Should be able to click on all tabs and ensure content and that tab menu is highlighted.
2. Press log button and close it.
3. Press log button and ensure each use case works.

## Find Hunts

1. Click on “Find Hunts” tab menu button and ensure content. Empty is a valid option.
2. Toggle between map and list view.
3. Select the first hunt in the list. 
4. Select the last hunt in the list.
5. Select a any hunt from map view.
6. Create new hunt and publish it.
7. Book a hunt with one seat and type in a message to the seller and go to payment. 
8. Book a hunt with two seats and type in a message to the seller and go to payment.
9. Book a hunt with one seat as a new customer.
10. Book a hunt with one seat as an existing customer, e.g. select the stored credit card.
11. Book a hunt with wrong credit card information (see https://stripe.com/docs/testing)

## Misc

1. Toggle language from settings view.
2. Go to your profile and click on “my hunts” button.
3. Navigate in the app. E.g. navigate into different use cases and press back button, to go back to original location.

## Messaging

1. Send a private message to a friend.
2. Receive a private message to a friend.
3. Live chat (the user doesn’t need to refresh to see a new message from the conversation).
4. Pressing the push notification received from a private message redirects you to your conversation with the user who sent the private message.

# Android 6

## Login

1. Email login - with correct credentials.
2. Email login - with wrong credentials.
3. Login with Facebook.
4. Go to settings view and logout.

## Feed

1. Select a feed item that has no likes and like it. 
2. Unlike a feed item.
3. Add a comment to any feed item.
4. Add a comment to another feed item and remove it.
5. Report a user but click cancel button.
6. Scroll down the list to fetch more feed items.

## Tab menu

1. Should be able to click on all tabs and ensure content and that tab menu is highlighted.
2. Press log button and close it.
3. Press log button and ensure each use case works.

## Find Hunts

1. Click on “Find Hunts” tab menu button and ensure content. Empty is a valid option.
2. Toggle between map and list view.
3. Select the first hunt in the list. 
4. Select the last hunt in the list.
5. Select a any hunt from map view.
6. Create new hunt and publish it.
7. Book a hunt with one seat and type in a message to the seller and go to payment. 
8. Book a hunt with two seats and type in a message to the seller and go to payment.
9. Book a hunt with one seat as a new customer.
10. Book a hunt with one seat as an existing customer, e.g. select the stored credit card.
11. Book a hunt with wrong credit card information (see https://stripe.com/docs/testing)

## Misc

1. Toggle language from settings view.
2. Go to your profile and click on “my hunts” button.
3. Navigate in the app. E.g. navigate into different use cases and press back button, to go back to original location. 

## Messaging

1. Send a private message to a friend.
2. Receive a private message to a friend.
3. Live chat (the user doesn’t need to refresh to see a new message from the conversation).
4. Pressing the push notification received from a private message redirects you to your conversation with the user who sent the private message. 

# Asus Zenfone 2 (Running Android 5.0)

## Login

1. Email login - with correct credentials.
2. Email login - with wrong credentials.
3. Login with Facebook.
4. Go to settings view and logout.

## Feed

1. Select a feed item that has no likes and like it. 
2. Unlike a feed item.
3. Add a comment to any feed item.
4. Add a comment to another feed item and remove it.
5. Report a user but click cancel button.
6. Scroll down the list to fetch more feed items.

## Tab menu

1. Should be able to click on all tabs and ensure content and that tab menu is highlighted.
2. Press log button and close it.
3. Press log button and ensure each use case works.

## Find Hunts

1. Click on “Find Hunts” tab menu button and ensure content. Empty is a valid option.
2. Toggle between map and list view.
3. Select the first hunt in the list. 
4. Select the last hunt in the list.
5. Select a any hunt from map view.
6. Create new hunt and publish it.
7. Book a hunt with one seat and type in a message to the seller and go to payment. 
8. Book a hunt with two seats and type in a message to the seller and go to payment.
9. Book a hunt with one seat as a new customer.
10. Book a hunt with one seat as an existing customer, e.g. select the stored credit card.
11. Book a hunt with wrong credit card information (see https://stripe.com/docs/testing)

## Misc

1. Toggle language from settings view.
2. Go to your profile and click on “my hunts” button.
3. Navigate in the app. E.g. navigate into different use cases and press back button, to go back to original location. 

## Messaging

1. Send a private message to a friend.
2. Receive a private message to a friend.
3. Live chat (the user doesn’t need to refresh to see a new message from the conversation).
4. Pressing the push notification received from a private message redirects you to your conversation with the user who sent the private message.

# Chrome (version 55.0.2883.87 m (64-bit))

## Login

1. Email login - with correct credentials.
2. Email login - with wrong credentials.
3. Login with Facebook.
4. Go to settings view and logout.

## Feed

1. Select a feed item that has no likes and like it. 
2. Unlike a feed item.
3. Add a comment to any feed item.
4. Add a comment to another feed item and remove it.
5. Report a user but click cancel button.
6. Scroll down the list to fetch more feed items.

## Tab menu

1. Should be able to click on all tabs and ensure content and that tab menu is highlighted.
2. Press log button and close it.
3. Press log button and ensure each use case works.

## Find Hunts

1. Click on “Find Hunts” tab menu button and ensure content. Empty is a valid option.
2. Toggle between map and list view.
3. Select the first hunt in the list. 
4. Select the last hunt in the list.
5. Select a any hunt from map view.
6. Create new hunt and publish it.
7. Book a hunt with one seat and type in a message to the seller and go to payment. 
8. Book a hunt with two seats and type in a message to the seller and go to payment.
9. Book a hunt with one seat as a new customer.
10. Book a hunt with one seat as an existing customer, e.g. select the stored credit card.
11. Book a hunt with wrong credit card information (see https://stripe.com/docs/testing)

## Misc

1. Toggle language from settings view.
2. Go to your profile and click on “my hunts” button.
3. Navigate in the app. E.g. navigate into different use cases and press back button, to go back to original location. 
4. Messaging
5. Send a private message to a friend.
6. Receive a private message to a friend.
7. Live chat (the user doesn’t need to refresh to see a new message from the conversation).

# Firefox (version 50.0.2)

## Login

1. Email login - with correct credentials.
2. Email login - with wrong credentials.
3. Login with Facebook.
4. Go to settings view and logout.

## Feed

1. Select a feed item that has no likes and like it. 
2. Unlike a feed item.
3. Add a comment to any feed item.
4. Add a comment to another feed item and remove it.
5. Report a user but click cancel button.
6. Scroll down the list to fetch more feed items.

## Tab menu

1. Should be able to click on all tabs and ensure content and that tab menu is highlighted.
2. Press log button and close it.
3. Press log button and ensure each use case works.

## Find Hunts

1. Click on “Find Hunts” tab menu button and ensure content. Empty is a valid option.
2. Toggle between map and list view.
3. Select the first hunt in the list. 
4. Select the last hunt in the list.
5. Select a any hunt from map view.
6. Create new hunt and publish it.
7. Book a hunt with one seat and type in a message to the seller and go to payment. 
8. Book a hunt with two seats and type in a message to the seller and go to payment.
9. Book a hunt with one seat as a new customer.
10. Book a hunt with one seat as an existing customer, e.g. select the stored credit card.
11. Book a hunt with wrong credit card information (see https://stripe.com/docs/testing)

## Misc

1. Toggle language from settings view.
2. Go to your profile and click on “my hunts” button.
3. Navigate in the app. E.g. navigate into different use cases and press back button, to go back to original location. 


## Messaging

1. Send a private message to a friend.
2. Receive a private message to a friend.
3. Live chat (the user doesn’t need to refresh to see a new message from the conversation).