# tgw-app
This is the application's front end! 

## Modules
* **ads:**
* **auth:**
* **invite:**
* **maps:**
* **notifications:**
* **profile:**
* **recommendation:**
* **reports:**
* **settings:**
* **teams:**
* **utils:**

## Meta tags
The application uses a library called [ngMeta](https://github.com/vinaygopinath/ngMeta) in order to be able to set each page's meta tags dynamically (meta tags are used for search engine optimization and to make sure content shared on social media is displayed correctly).

In order to add a new meta tag, for example author, first add it to the header section of index.html:   
`<meta name="author" content="{{ngMeta['author']}}" />`

Then set the default value in app.ctrl.js:   
`ngMetaProvider.setDefaultTag('author', 'Rob Halford');`

The meta tag value for a specific page can either be set switching state to that page, by adding a meta section in the following way:

angular.module('TGW.ads').config(function($stateProvider, ngMetaProvider) {

`angular.module('TGW.ads').config(function($stateProvider, ngMetaProvider) {   
    $stateProvider   
        .state('create_hunt', {   
            abstract: true,   
            url: '/hunt',   
            templateUrl: 'partials/create-ad.html',   
            controller: 'ManageAdController',   
            meta: {   
                'author': 'Bruce Dickinson'   
            }   
        });   
});`

Or, alternatively, be set from the controller:

`.controller('ManageAdController', function(ngMeta) {
    ngMeta.setTag('author', 'Dave Mustaine');
});`
