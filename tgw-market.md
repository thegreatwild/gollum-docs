# tgw-market

TGW's Java backend.

---

## Architecture
![Java architecture](images/java_architecture.png)

---
## Testing
Always write unit tests for all new features, and make sure all tests still pass when you implement new features!

You can configure the tests using the following patterns in IntelliJ (sett "use classpath of module" to huntig-ad):
* System tests: `com.tgw.*Test|com.SystemTestsSuite`
* Integration tests: `com.tgw.*Test|com.IntegrationTestsSuite`
* All tests: `com.tgw.*Test|com.IntegrationTestsSuite|com.SystemTestsSuite`

---

## Dependencies
The backend uses the following dependencies (listed in pom.xml):
* **[Guice](https://github.com/google/guice/wiki/GettingStarted):** Google's [dependency injection](http://stackoverflow.com/a/130862/948942) framework.
* **[RxJava](https://github.com/ReactiveX/RxJava/wiki/How-To-Use-RxJava)**: A library for asynchronous event handling.
* **[Jetty](https://eclipse.org/jetty/)**: A web server and servlet container. 
* **[Spark](http://sparkjava.com/documentation.html)**: A web framework for Java (used to handle HTTP requests etc).
* **[Vert.x](http://vertx.io/docs/)**: A toolkit for building reactive and event-driven applications.
   * **[Vert.x-Web](http://vertx.io/docs/vertx-web/js/)**: Building blocks for web applications. See some examples [here](https://github.com/vert-x3/vertx-examples/tree/master/web-examples).
   * **[Hazelcast](http://vertx.io/docs/vertx-hazelcast/)**: Cluster management.
   * **[Vert.x RxJava](http://vertx.io/docs/vertx-rx/java/)**: Vert.x API for RxJava.
* **[Gson](https://github.com/google/gson/blob/master/UserGuide.md#primitives-examples)**: Conversion between Java objects and JSON.
* **[Hibernate](http://docs.jboss.org/hibernate/orm/4.3/quickstart/en-US/html_single/)**: An [ORM](http://hibernate.org/orm/what-is-an-orm/) framework.
* **[c3p0](http://www.mchange.com/projects/c3p0/)**: 
* **[MySQL](http://dev.mysql.com/)**: A database.
* **[slf4j](http://www.slf4j.org/manual.html)**: The Simple Logging Facade for Java, makes it possible to plug in some logging framework at deployment time.
* **[Jedis](https://github.com/xetorthio/jedis)**: A small [Redis](http://redis.io/) client.
* **[FindBugs](http://findbugs.sourceforge.net/)**: Used to find bugs in the Java code.
* **[SendGrid-Java](https://github.com/sendgrid/sendgrid-java#sendgrid-java)**: A module providing functionality for sending emails. 
* **[Stripe-Java](https://github.com/stripe/stripe-java#usage)**: API for credit card payments.
* **[AWS SDK](http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/AboutJava.html)**: API for accessing the [DynamoDB](http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html) hosted by [AWS (Amazon Web Services)](https://aws.amazon.com/).
* **[Java-APNS](https://github.com/notnoop/java-apns)**: Java [Apple Push Notification Service](https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ApplePushService.html) Provider.
* **[gcm4j](https://github.com/phonedeck/gcm4j#getting-started)**: A Java [Google Cloud Messaging (GCM)](https://developers.google.com/cloud-messaging/) Client.
* **[Lombok](https://projectlombok.org/)**: Automatically generates code (such as setters and getters) from simple annotations. 


Here are the dependencies used for testing:

* **[JUnit](https://github.com/junit-team/junit/wiki/Getting-started)**: Unit testing framework.
* **[AssertJ](http://joel-costigliola.github.io/assertj/)**: Java assertions.
* **[Jersey](https://jersey.java.net/documentation/latest/getting-started.html)**: RESTful Web Services in Java.
* **[Fishbowl](https://github.com/stefanbirkner/fishbowl)**: Provides helper methods for dealing with exceptions.
* **[Mockito](http://site.mockito.org/mockito/docs/current/org/mockito/Mockito.html)**: A testing framework that enables the use of mock objects.
* **[JUnit Toolbox](https://github.com/MichaelTamm/junit-toolbox)**: Useful JUnit helper functions. 
* **[Vertx Unit](http://vertx.io/docs/vertx-unit/java/)**: Asynchronous unit testing.

---

Plugins:

* **[Maven Surefire Plugin](https://maven.apache.org/surefire/maven-surefire-plugin/)**: Used to execute the unit tests.

---

## Logging
To get the logging tool to work in IntelliJ, you need to manually install the [Lombok plugin](https://github.com/mplushnikov/lombok-intellij-plugin) in IntelliJ.

Add the @Slf4j annotation at the top of any class in which you want to use logging.


---
