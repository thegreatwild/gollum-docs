# Notifications
A documentation on how notifications, in-app or external(on-device), are implemented.

## In-App Badges
Below is an example where there is a new activity and a count, the badge, is displayed over the _Activity_ menu item.
![main-menu-badge](images/main-menu-badge.png)

* See **mainMenu** directive in `tgw-app/src/app/main-menu/main-menu.dir.js`. 
* The _Profile_ menu item has a badge that is bound to `unreadMessageCount` while the _Activity_ badge is bound to `badge`. See `$app.on('unreadMessageCount')` and `$app.on('remoteUpdate')`.
* **MessagingService**, in `tgw-app/src/modules/messaging/messaging-service.js`, does the `$app.trigger('unreadMessageCount', count);`.
* **NotificationController**, in `tgw-app/src/modules/notifications/notifications.js`, does the `$app.trigger('clearBadgeNumber');`.
* **ProfileController** also has a `unreadMessageCount` for the inbox which also is handled on `$app.on('unreadMessageCount')`.
![profile-inbox-badge](images/profile-badge.png)

## Push Notifications Initialization/Registration
Using [phonegap-plugin-push](https://github.com/phonegap/phonegap-plugin-push)
<br> **pushService**

* The plugin is initialized by **pushService**, `tgw-app/src/modules/notifications/push-service.js`
* **AppCtrl** in `tgw-app/src/app/app.ctrl.js` : `$app.onDeviceReady` → `pushService.init();`
* pushService: `init()` → `push.on('registration')` → `$app.trigger('pushService:registration', pushData);`

**MessagingService**

* Google Cloud Messaging and Apple Push Notification Service Ids are taken from the **pushService** (`gcmSenderId` and `apnsSenderId`).
* `$app.on('pushService:registration', postPushData);`  → Register push token to layer : `postPushData(data)`
![profile-inbox-badge](images/pushService-registration.png)

## [notification.py](http://stash.thegreatwild.com/projects/TGW/repos/tgw-back/browse/src/models/notification.py)
`tgw-back\src\models\notification.py`
<br>Sending of Notifications is handled by <font color="#ff9400"><strong>Notification</strong></font> class.

## User Actions or Events that Trigger/Send Notifications
The following are actions that can trigger the sending of Notifications. See **[tgw-back](http://stash.thegreatwild.com/projects/TGW/repos/tgw-back/browse)**.

* **User Posts a Report (Trophy or Obesrvation)**
   * Posting a Trophy or an Observation  is handled by <font color="#ff9400"><strong>MeReportsHandler</strong></font> class at `tgw-back\src\handlers\report.py`.
   * Sends notifications to the user's friends with <font color="#ff9400"><strong>Notification</strong></font>**.send_to_friends(**...**)**.
* **User Posts a Status**
   * Posting a Status is handled by <font color="#ff9400"><strong>PostsHandler</strong></font> class at `tgw-back\src\handlers\post.py`.
   * Sends notifications to the user's friends with <font color="#ff9400"><strong>Notification</strong></font>**.send_to_friends(**...**)**.
* **User Sends or Accepts a Friend Request**
   * Sends a notification to a user accepted as friend or a recipient of a friend request.
   * Handled by <font color="#ff9400"><strong>FriendRequestsHandler</strong></font> class at `tgw-back\src\handlers\friends.py`.
* **User Comments to a Post**
   * Commenting to a Status is handled by <font color="#ff9400"><strong>CommentFeedHandler</strong></font> class at `tgw-back\src\handlers\comment.py`.
   * Commenting to a Trophy or Observation is handled by <font color="#ff9400"><strong>CommentReportHandler</strong></font>.
* **User Likes a Post**
   * See <font color="#ff9400"><strong>LikeReportHandler</strong></font> class at `tgw-back\src\handlers\like.py`
   * See <font color="#ff9400"><strong>LikeFeedHandler</strong></font> class at `tgw-back\src\handlers\like.py`
* **When Licenses have updated**
   * See <font color="#ff9400"><strong>LicenseHandler</strong></font> class at `tgw-back\src\handlers\license.py`
* **Admin Sends a Custom Notification**
   * See <font color="#ff9400"><strong>NotificationsHandler</strong></font> class at `tgw-back\src\handlers\notification.py`
* **User Posts to a Hunting Team Feed**
   *  Posting to a Hunting Team Feed will send notifications to the members. See <font color="#ff9400"><strong>GroupsFeedHandler</strong></font> class at `tgw-back\src\handlers\group.py`
* **User Joined a Hunting Team**
   *  When a user has joined a Hunting Team,notifications to the that user and the members. See <font color="#ff9400"><strong>GroupsFeedHandler</strong></font> class at `tgw-back\src\handlers\group.py`
* **User is Removed from a Hunting Team**
   *  Removing a user from a Hunting Team sends a notification to the that user. See <font color="#ff9400"><strong>GroupsFeedHandler</strong></font> class at `tgw-back\src\handlers\group.py`