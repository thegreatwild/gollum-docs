# Trouble shooting
Frequently unsolved problems and their solutions [if any] are to be found here

## Cordova Build
### Wrong Icons on the App for Android

**Problem:**
Icons in the app are using the default Cordova Robot Icon 

**Cause:**
Icons are not copied to Android's platform/android/res folder despite the Cordova Documentation as of v6.4.0

**Solution:**
It's a Cordova issue and there's a bug report [here](https://issues.apache.org/jira/browse/CB-12160).
Cordova v6.4.0 with Cordova Android@6.0.0 doesn't copy the icons to the destination, this was fixed in Cordova Android@6.1.0 so try updating Cordova or Cordova Android platform. As of the date when this problem occurred, updating to Android@6.1.0 fixed it.

## Web app
### Strange CORS exceptions

Solved by disabling browser caching in your browser's dev settings.

## Local environment
### Docker

__Unable to mount volumes__ -> Sometimes the script "/mnt/.../boot2docker/bootlocal.sh" becomes corrupted -> Solved by re-adding the file & restarting docker-machine until it stops corrupting the file.
### TGW-market
__Hibernate stops communicating with MySQL__ -> Solved by stopping all docker containers manually, run docker-compose for TGW-market & TGW-back to get the environment up again.

## Build problems
Facebook-login does not work (iOS): Rerun deploy-script and choose to reinstall the entire project (reason: perhaps the facebook-plugin was not installed correctly due to lost internet connection for example).

Facebook-login does not work (Android): Add the hash of the build computer to the project on developers.facebook.com