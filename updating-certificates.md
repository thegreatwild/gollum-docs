* Updating Certificates for Push Notifications and Deploying to Elastic Beanstalk
* * Locate certificates at /src/main/resources
* * Remove the old/expired dev.apns-cert.p12 and prod.apns-cert.p12
* * Copy and put the updated .p12 certificates
* * Using docker terminal, go to \tgw\tgw-market\hunting-ad\docker\setup and run the script 'docker-compose up'
* * The script command will build the java market solution and produce an appropriate 'hunting-ad-*.war' on /target folder
* * Use this .WAR file and ensure a copy is created on /docker folder. Rename it to ROOT.war
* * At /docker folder, three (3) items Dockerfile, Dockerrun.aws, and ROOT.war are bundled or zipped together. Enter an appropriate name for the zipped file.
* * Go to AWS Elastic Beanstalk. 
* * On the Dashboard, locate the 'Upload and Deploy' button under the heading 'Running Version'
* * On the Upload and Deploy modal, browse and locate the bundled/zipped folder created earlier. Then hit deploy
* * On the 'Configuration' tab, under 'Software Configuration', update the APPLE_PUSH_CERTIFICATE_PASSWORD to match the latest value assigned when creating the new set of certificates, otherwise, the application generates SSL related exceptions.
* * NOTE: before doing this process, make sure timing is appropriate as this procedure will restart the application and perform the bootstrap/instantiation of the updated version. Any access to the application could generate 504s or similar as the application is still starting up.
