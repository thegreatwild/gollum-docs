# Work flow
The TGW project uses the following work flow.

1. Before you start working on a new feature/task, mark the corresponding task as "in progress" in [Jira](http://jira.thegreatwild.com/).
2. Next, click "Create branch" from that task in Jira (See [Git model](http://gitlab.kicksort.se/tgw/wiki/wikis/work-flow#git-model) below for more information). 
3. From your console, run `git fetch` to retrieve the new branch and then `git checkout <branch-name>` to switch to that branch.
4. Implement the feature!
5. Make sure that the documentation in [app features](app-features) is up to date.
6. Finally, you can merge your feature branch back into to the develop branch.
   1. Go into the branch in [Stash](http://stash.thegreatwild.com/projects/TGW).
   2. Create pull request (into the develop branch).
   3. Select at least one other team member to review your changes.
   4. Move the task to "Review" in Jira.
   5. When the code review is done, you can merge the branches.
   6. Delete the source branch.
   7. Mark the task as done in Jira!
   8. Mark the task with the corresponding version 

## Git model
We basically use four kinds of branches in each of our repos:
* A **master** branch (HEAD represents the latest production ready version). 
* A **develop** branch.
* A **feature** branch for each task in Jira. Each feature branch should branch off from the develop branch. 
* A **release** branch (also branched off from the develop branch) for when the code is prepared for a new release. When the release have been built and put into production, the last commit should be tagged with the version number, and then the branch is merged back into the master branch. 

See [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/) for more information.


## tgw-market
tgw-market is developed using a test-driven development approach. This means that **before** you add a new feature, you have to write unit tests that can verify when the implementation works as intended!